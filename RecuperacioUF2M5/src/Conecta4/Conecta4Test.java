package Conecta4;

import java.util.Scanner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Conecta4Test {

    private Conecta4 conecta4;

    @Before
    public void setUp() {
        conecta4 = new Conecta4();
    }

    @Test
    public void testRealizarMovimient() {
        conecta4.configurarJuego(new Scanner(System.in));
        conecta4.realizarMovimient(new Scanner("1"), 'X');
        char[][] tauler = conecta4.tauler;
        Assert.assertEquals('X', tauler[5][0]);
    }

    @Test
    public void testComprobarGuanyador_Fila() {
        conecta4.configurarJuego(new Scanner(System.in));
        conecta4.tauler[5][0] = 'X';
        conecta4.tauler[5][1] = 'X';
        conecta4.tauler[5][2] = 'X';
        conecta4.tauler[5][3] = 'X';
        boolean guanyador = conecta4.comprobarGuanyador('X');
        Assert.assertTrue(guanyador);
    }

    @Test
    public void testComprobarGuanyador_Columna() {
        conecta4.configurarJuego(new Scanner(System.in));
        conecta4.tauler[5][0] = 'X';
        conecta4.tauler[4][0] = 'X';
        conecta4.tauler[3][0] = 'X';
        conecta4.tauler[2][0] = 'X';
        boolean guanyador = conecta4.comprobarGuanyador('X');
        Assert.assertTrue(guanyador);
    }

    @Test
    public void testComprobarGuanyador_Diagonal1() {
        conecta4.configurarJuego(new Scanner(System.in));
        conecta4.tauler[5][0] = 'X';
        conecta4.tauler[4][1] = 'X';
        conecta4.tauler[3][2] = 'X';
        conecta4.tauler[2][3] = 'X';
        boolean guanyador = conecta4.comprobarGuanyador('X');
        Assert.assertTrue(guanyador);
    }

    @Test
    public void testComprobarGuanyador_Diagonal2() {
        conecta4.configurarJuego(new Scanner(System.in));
        conecta4.tauler[2][0] = 'X';
        conecta4.tauler[3][1] = 'X';
        conecta4.tauler[4][2] = 'X';
        conecta4.tauler[5][3] = 'X';
        boolean guanyador = conecta4.comprobarGuanyador('X');
        Assert.assertTrue(guanyador);
    }
}
