/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Conecta4;

/**
 *
 * @author Alberto Cobo
 */
import java.util.Scanner;

public class Conecta4 {

	public static char[][] tauler;
	public static int files;
	public static int columnes;
	public static String jugador1;
	public static String jugador2;
	public static String guanyador;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int opcion;

		do {
			System.out.println("Menú:");
			System.out.println("1.- Mostrar Ajuda");
			System.out.println("2.- Opcions");
			System.out.println("3.- Jugar Partida");
			System.out.println("4.- Veure Ranking");
			System.out.println("0.- Sortir");

			opcion = scanner.nextInt();

			switch (opcion) {
			case 1:
				mostrarAyuda();
				break;
			case 2:
				configurarJuego(scanner);
				break;
			case 3:
				jugarPartida(scanner);
				break;
			case 4:
				veureRanking();
				break;
			case 0:
				System.out.println("Gràcies per jugar! Fins la pròxima!");
				break;
			default:
				System.out.println("Opció invàlida. Si us plau, tria una opció vàlida.");
				break;
			}

			System.out.println();
		} while (opcion != 0);

		scanner.close();
	}

	public static void mostrarAyuda() {
		System.out.println(
				"El joc de Conecta 4 consisteix en col·locar quatre fitxes en una fila continua vertical, horitzontal o diagonalment. Es juga sobre un tauler de NxM caselles que inicialment està buit.");
		System.out.println(
				"Els dos jugadors situen les seves fitxes (una per moviment) al tauler. Les fitxes sempre cauen fins a baix.");
		System.out.println(
				"La partida acaba si un dels jugadors col·loca quatre o més fitxes en una línia contínua vertical, horitzontal o diagonalment, o si totes les caselles del tauler estan ocupades sense cap jugador guanyador (empat).");
	}

	public static void configurarJuego(Scanner scanner) {
		System.out.print("Nom del primer jugador: ");
		jugador1 = scanner.next();
		System.out.print("Nom del segon jugador: ");
		jugador2 = scanner.next();
		System.out.print("Número de files: ");
		files = scanner.nextInt();
		System.out.print("Número de columnes: ");
		columnes = scanner.nextInt();

		tauler = new char[files][columnes];
	}

	public static void jugarPartida(Scanner scanner) {
		if (jugador1 == null || jugador2 == null || files == 0 || columnes == 0) {
			jugador1 = "Jugador 1";
			jugador2 = "Jugador 2";
			files = 6;
			columnes = 7;
			tauler = new char[files][columnes];
		}

		// Reiniciar el tauler
		reiniciarTauler();

		boolean jocTerminat = false;
		boolean turnoJugador1 = true;
		int movimients = 0;

		while (!jocTerminat) {
			imprimirTauler();

			if (turnoJugador1) {
				System.out.println("Turno de " + jugador1);
				realizarMovimient(scanner, 'X');
			} else {
				System.out.println("Turno de " + jugador2);
				realizarMovimient(scanner, 'O');
			}

			movimients++;

			if (comprobarGuanyador('X')) {
				jocTerminat = true;
				guanyador = jugador1;
			} else if (comprobarGuanyador('O')) {
				jocTerminat = true;
				guanyador = jugador2;
			} else if (movimients == files * columnes) {
				jocTerminat = true;
				guanyador = "Empat";
			}

			turnoJugador1 = !turnoJugador1;
		}

		System.out.println("Joc finalitzat!");
		imprimirTauler();
		if (!guanyador.equals("Empat")) {
			System.out.println("El guanyador és: " + guanyador);
		} else {
			System.out.println("La partida ha acabat en empat.");
		}
	}

	public static void reiniciarTauler() {
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				tauler[i][j] = '-';
			}
		}
	}

	public static void imprimirTauler() {
		System.out.println("-------------");
		for (int i = 0; i < files; i++) {
			for (int j = 0; j < columnes; j++) {
				System.out.print(tauler[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("-------------");
	}

	public static void realizarMovimient(Scanner scanner, char fitxa) {
		boolean movimientValido = false;
		int columna = -1;

		while (!movimientValido) {
			System.out.print("Introdueix el número de columna (1-" + columnes + "): ");
			columna = scanner.nextInt() - 1;

			if (columna >= 0 && columna < columnes && tauler[0][columna] == '-') {
				movimientValido = true;
			} else {
				System.out.println("Moviment invàlid. Si us plau, introdueix un número de columna vàlid.");
			}
		}

		for (int i = files - 1; i >= 0; i--) {
			if (tauler[i][columna] == '-') {
				tauler[i][columna] = fitxa;
				break;
			}
		}
	}

	public static boolean comprobarGuanyador(char fitxa) {
		// Comprobar files
		for (int i = 0; i < files; i++) {
			for (int j = 0; j <= columnes - 4; j++) {
				if (tauler[i][j] == fitxa && tauler[i][j + 1] == fitxa && tauler[i][j + 2] == fitxa
						&& tauler[i][j + 3] == fitxa) {
					return true;
				}
			}
		}

		// Comprobar columnas
		for (int j = 0; j < columnes; j++) {
			for (int i = 0; i <= files - 4; i++) {
				if (tauler[i][j] == fitxa && tauler[i + 1][j] == fitxa && tauler[i + 2][j] == fitxa
						&& tauler[i + 3][j] == fitxa) {
					return true;
				}
			}
		}

		
		for (int i = 3; i < files; i++) {
			for (int j = 0; j <= columnes - 4; j++) {
				if (tauler[i][j] == fitxa && tauler[i - 1][j + 1] == fitxa && tauler[i - 2][j + 2] == fitxa
						&& tauler[i - 3][j + 3] == fitxa) {
					return true;
				}
			}
		}

		
		for (int i = 0; i <= files - 4; i++) {
			for (int j = 0; j <= columnes - 4; j++) {
				if (tauler[i][j] == fitxa && tauler[i + 1][j + 1] == fitxa && tauler[i + 2][j + 2] == fitxa
						&& tauler[i + 3][j + 3] == fitxa) {
					return true;
				}
			}
		}

		return false;
	}

	public static void veureRanking() {
		if (guanyador != null) {
			System.out.println("Els guanyadors anteriors són:");
			System.out.println("- " + guanyador);
		} else {
			System.out.println("Encara no hi ha guanyadors registrats.");
		}
	}
}
